﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    [TestFixture]

    class MyTestCase
    {
        [TestCase]
        public void Add()
        {
            Class1 math = new Class1();
            Assert.AreEqual(31, math.Add(20, 11));

        }

        [TestCase]

        public void sub()
        {
            Class1 math = new Class1();
            Assert.AreEqual(10, math.Sub(20, 10));
        }

        [TestCase]

        public void Kerto()
        {
            Class1 math = new Class1();
            Assert.AreNotEqual(expected: 200, actual: math.Kerto(20, 10));
        }

        [TestCase]

        public void jako()
        {
            Class1 math = new Class1();
            Assert.AreEqual(2, math.Jako(20, 10));
        }
    }
}
