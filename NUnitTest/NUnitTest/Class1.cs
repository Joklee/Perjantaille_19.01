﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NUnitTest
{
    class Class1
    {
        public int Add(int a, int b)
        {
            return a + b*2;

        }

        public int Sub(int a, int b)
        {
            return a - b;

        }

        public int Kerto(int a, int b)
        {
            return a * b;

        }

        public int Jako(int a, int b)
        {
            return a / b;

        }
    }
}
